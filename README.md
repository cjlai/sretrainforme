# 以成為Kubernetes SRE為終極目標的專案

![](https://img.shields.io/badge/Windows-11-blue) ![](https://img.shields.io/badge/RAM-32_GB-blue) ![](https://img.shields.io/badge/Disk-1T-blue)

這個專案主要是用來整理個人學習的環境，會依照我現在學習的技術不斷修改內容，有興趣可以作為參考自己建立自己專屬的環境。目前我的學習途徑依序從: `Docker`、`Kubernets`、`OpenShift`逐步涉獵。有任何疑問或建議都歡迎發issue或mr給我，惠請不吝指教，謝謝。

## 1. 使用先決條件
* Git
* VirtaulBox
* Vagrant

## 2. 提供使用環境
* CentOS7
* CentOS8
* K3d
* Kind
## 3. 如何使用環境
* 執行
```bash
$git clone https://gitlab.com/cjlai/sretrainforme.git
$cd sretrainforme/kind/
$vagrant up
$vagrant ssh
```

* 刪除
```bash
$pwd                     # 確認目錄
$vagrant destroy -f
```

* 確認狀態
```bash
$vagrant status          # 僅有在Vagrantfile當前目錄底下有效
$vagrant global-status   # 任意path都可以執行，方便用來確認現在執行哪些vagrantfile以及其位置
```