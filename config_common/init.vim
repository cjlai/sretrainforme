syntax enable

set number
set t_Co=256
set hlsearch
set ts=4
set expandtab
set autoindent
set encoding=utf-8
set inccommand=nosplit
set fileformat=unix

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_statusline_ontop=1


" 快捷鍵綁定
nmap <F3> :NERDTreeToggle<CR>

call plug#begin()
" 側邊欄
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }   

" 狀態列
Plug 'vim-airline/vim-airline' 
Plug 'vim-airline/vim-airline-themes'

" 註解
Plug 'preservim/nerdcommenter'

" Dockerfile高亮
Plug 'ekalinin/dockerfile.vim'

" go
Plug 'fatih/vim-go' , { 'do': ':GoInstallBinaries' }

Plug 'kyazdani42/nvim-web-devicons'
Plug 'projekt0n/circles.nvim'

" 主題
Plug 'projekt0n/github-nvim-theme'

call plug#end()

colorscheme github_dark

