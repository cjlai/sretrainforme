#!/bin/bash

work_nodes=$(kubectl get no --no-headers |grep -v master |awk '{print$1}')
for node in $work_nodes
do 
  echo "[info]" $node process... 
  kubectl label node/$node node-role.kubernetes.io/compute=
  kubectl label node/$node node-role.kubernetes.io/worker=
  echo ""
done 
